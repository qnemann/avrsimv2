#ifndef _AVRSIMV2_GUI_H
#define _AVRSIMV2_GUI_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <pthread.h>

typedef struct gui_t__ gui_t;

#include "avrsimv2.h"
#include "gl_utils.h"

struct gui_t__ {
    avrsimv2_t *s;
    bool floating;
    // GLFW
    GLFWwindow *window;
    double currentFrameTime; // sec
    double framesPerSecond;
    // FreeType
    FT_Library ft;
    struct {
        GLFont *monospace;
    } fonts;
};


void gui_init(gui_t *v);
void gui_loop(gui_t *v);
void gui_close(gui_t *v);




#endif //_AVRSIMV2_GUI_H