#ifndef AVRSIMV2_OPTS_H
#define AVRSIMV2_OPTS_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>


typedef enum {
    OPT_ARG_STRING,
    OPT_ARG_LONG,
    OPT_ARG_LONGLONG,
    OPT_ARG_ULONG,
    OPT_ARG_ULONGLONG,
    OPT_ARG_FLOAT,
    OPT_ARG_DOUBLE,
    OPT_ARG_LONGDOUBLE
} opt_arg_type_t;

typedef struct opt_arg_t__ opt_arg_t;
struct opt_arg_t__ {
    char *name;                     // nullable
    opt_arg_type_t type;            // default: string
    char *description;              // nullable
    void *target;                   // nullable
    int base;
    int (*callback)(int *argc, char ***argv, opt_arg_t arg);    // nullable
    void *customData;                                           // nullable
};

typedef struct opt_t__ opt_t;
struct opt_t__ {
    char sname;                     // may be 0
    char *lname;                    // nullable
    
    int minArgs;
    int maxArgs;
    opt_arg_t *args;
    
    char *description;              // nullable

    int *counter;                   // nullable
    int *flag;                      // nullable
    int flagVal;
    int (*callback)(int *argc, char ***argv, opt_t opt);    // nullable
    void *customData;                                       // nullable
};

typedef struct {
    int optc;
    opt_t *opts;
    
    int minArgs;
    int maxArgs;
    opt_arg_t *args;
} opts_t;

extern bool opts_debugOut;

#define OPT_SUCCESS     -3
#define OPT_ERROR       -2
#define OPT_ARG         -1

int opts_parseSingle(int *argc, char ***argv, int optc, opt_t *opts);
int opts_parse(int *argc, char ***argv, opts_t opts);

void opts_printUsage(opts_t opts);




#endif //AVRSIMV2_OPTS_H
