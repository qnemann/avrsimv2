#ifndef _GL_UTILS_H
#define _GL_UTILS_H

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define GL_GLEXT_PROTOTYPES // possibly needed for VBOs
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "utils/map.h"

typedef uint32_t GLcolor32;
typedef struct {
    GLfloat x, y;
} GLvec2f;
#define GLvec2f_ZERO ((GLvec2f) {0, 0})
#define COLOR_NULL          0x00000000
#define COLOR_TRANSPARENT   COLOR_NULL
#define COLOR_BLACK         0x000000FF
#define COLOR_GREY          0x888888FF
#define COLOR_WHITE         0xFFFFFFFF
#define COLOR_RED           0xFF0000FF
#define COLOR_GREEN         0x00FF00FF
#define COLOR_BLUE          0x0000FFFF
#define COLOR_CYAN          0x00FFFFFF
#define COLOR_MAGENTA       0xFF00FFFF
#define COLOR_YELLOW        0xFFFF00FF



typedef struct {
    GLvec2f off;
    GLvec2f advance;
} GLFontGlyphMetricsDirection;

typedef struct {
    GLvec2f size;
    GLFontGlyphMetricsDirection hori;
    GLFontGlyphMetricsDirection vert;
} GLFontGlyphMetrics;

typedef struct {
    wchar_t code;
    GLuint texture;
    GLFontGlyphMetrics metrics;
} GLFontGlyph;

MAP_DECL_TYPES(glyphs, wchar_t, GLFontGlyph *);

typedef enum {
    FONT_HORIZONTAL,
    FONT_VERTICAL
} GLFontOrientation;

typedef struct {
    FT_Face face;
    GLvec2f ppu;
    MAP(glyphs) glyphCache;
    GLFontGlyph *glyph;
    GLcolor32 color;
    GLFontOrientation orientation;
    GLfloat sizeFactor;
} GLFont;

MAP_DECL_FUNCS(glyphs);







void checkGLError(); // TODO deprecated through GLFW error callback (maybe?)
void glColor32(GLcolor32 color);
void glDrawCircle(GLvec2f pos, float radius);
void glOutlineRectf(GLvec2f size);



//GLvec2f GLvec2f_add(GLvec2f v1, GLvec2f v2) {
//    return (GLvec2f) {v1.x + v2.x, v1.y + v2.y};
//}
//GLvec2f GLvec2f_scale(GLfloat s, GLvec2f v) {
//    return (GLvec2f) {s * v.x, s * v.y};
//}





#define glLBeginOnDraw do { if (glLIsDrawing()) { (void) 0
#define glLEndOnDraw } } while (0)
#define glLBeginOnNotDraw do { if (!glLIsDrawing()) { (void) 0
#define glLEndOnNotDraw } } while (0)
#define glLOnDraw(draw) glLBeginOnDraw; draw; glLEndOnDraw

#define glLPush glLOnDraw(glPushMatrix())
#define glLPop glLOnDraw(glPopMatrix())
#define glLTranslatef(x, y) glLOnDraw(glTranslatef(x, y, 0))


void glLBeginDraw(void);
GLvec2f glLEndDraw(void);
void glLBeginMeasure(void);
GLvec2f glLEndMeasure(void);
bool glLIsDrawing(void);
GLFont *glLGetFont(void);
void glLSetFont(GLFont *font);
GLcolor32 glLGetDebug(void);
void glLSetDebug(GLcolor32 debug);

void glLBeginObject(void);
void glLEndObject(void);
void glLBeginVertical(void);
void glLEndVertical(void);
void glLBeginHorizontal(void);
void glLEndHorizontal(void);

void glLMinSize(GLvec2f v);
void glLSpacer(GLfloat s);

#define glLSimpleObject(draw, size) do { \
    glLBeginObject();                    \
    glLBeginOnDraw;                      \
    draw;                                \
    glLEndOnDraw;                        \
    glLMinSize(size);                    \
    glLEndObject();                      \
} while (0)
#define glLTextF(format, ...) do { \
    glLBeginObject();              \
    glLMinSize(glFontDrawTextF(glLGetFont(), format, ##__VA_ARGS__)); \
    glLEndObject(); \
} while (0)
#define glLDebug(color, what) do { \
    glLSetDebug(color); \
    what; \
    glLSetDebug(false); \
} while (0)


GLFont *glFontLoad(FT_Library ft, char *file, FT_UInt pixelSize);
void glFontClose(GLFont **f);
void glFontSetColor(GLFont *f, GLcolor32 color);
void glFontSetSizeFactor(GLFont *f, GLfloat sizeFactor);

GLFontGlyph *glFontLoadGlyph(GLFont *f, wchar_t code);
GLFontGlyphMetrics *glFontLoadGlyphMetrics(GLFont *f, wchar_t code);
GLvec2f glFontNewLine(GLFont *f);
GLvec2f glFontDrawGlyph(GLFont *f, wchar_t code);
GLvec2f glFontDrawTextW(GLFont *f, wchar_t *text);
GLvec2f glFontDrawTextF(GLFont *f, const char *format, ...);



#endif //_GL_UTILS_H
