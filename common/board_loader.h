#ifndef _BOARD_LOADER_H
#define _BOARD_LOADER_H

#include "board.h"

board_t *board_load(avrsimv2_t *s, char *path, int argc, char *argv[]);
void board_unload(board_t **board);

#endif //_BOARD_LOADER_H
