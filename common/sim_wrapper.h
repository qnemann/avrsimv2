#ifndef _SIM_WRAPPER_H
#define _SIM_WRAPPER_H

#include <stdint.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <pthread.h>
#include <simavr/sim_avr.h>
#include <simavr/sim_elf.h>

typedef struct sim_t__ sim_t;

#include "avrsimv2.h"

struct sim_t__ {
    avrsimv2_t *s;
    
    const char *mmcu;
    uint32_t frequency;
    elf_firmware_t firmware;
    uint16_t gdbPort;
    
    avr_t *avr;
    pthread_t thread;
    
    /* atomic_bool */ bool tryCTCorrection;
    /* atomic_bool */ bool isDone;
    /* atomic_uint_fast64_t */ uint64_t targetCycleDelta; // ns
    /* atomic_uint_fast64_t */ uint64_t lastCycleDelta; // ns
    /* atomic_uint_fast64_t */ uint64_t currentTime; // ns
    uint64_t startTime; // ns
};

void sim_init(sim_t *sim);
void sim_start(sim_t *sim);
void sim_reset(sim_t *sim);
void sim_waitTillDone(sim_t *sim);

#endif //_SIM_WRAPPER_H
