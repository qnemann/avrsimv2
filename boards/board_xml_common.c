#ifndef _BOARD_XML_COMMON_C
#define _BOARD_XML_COMMON_C


#include <libxml/tree.h>
#include <libxml/parser.h>

#include "../common/board.h"

#define BXML_PREFIX "Board XML"

#define BXML_TRACE(f, ...) L_TRACE(BXML_PREFIX": "f, ##__VA_ARGS__)
#define BXML_DEBUG(f, ...) L_DEBUG(BXML_PREFIX": "f, ##__VA_ARGS__)
#define BXML_INFO(f, ...) L_INFO(BXML_PREFIX": "f, ##__VA_ARGS__)
#define BXML_WARNING(f, ...) L_WARNING(BXML_PREFIX": "f, ##__VA_ARGS__)
#define BXML_ERROR(f, ...) L_ERROR(BXML_PREFIX": "f, ##__VA_ARGS__)
#define BXML_ERROR_EXIT(s, f, ...) L_ERROR_EXIT(s, BXML_PREFIX": "f, ##__VA_ARGS__)



typedef struct board_xml_t__ board_xml_t;


// region XML Utils

static bool xmlUtilsEqualsStr(const xmlChar *xml, const char *str) {
    return !strcmp((const char *) xml, str);
}
static char *xmlUtilsCopyStr(const xmlChar *str) {
    char *copy = malloc(strlen((const char *) str) + 1);
    strcpy(copy, (const char *) str);
    return copy;
}
static xmlAttr *xmlUtilsFindAttr(xmlNode *node, const char *name) {
    if (!node) return NULL;
    xmlAttr *attr = node->properties;
    while (attr) {
        if (xmlUtilsEqualsStr(attr->name, name)) return attr;
        attr = attr->next;
    }
    return NULL;
}
static xmlNode *xmlUtilsFindAttrVal(xmlNode *node, const char *name) {
    if (!node) return NULL;
    xmlAttr *attr = xmlUtilsFindAttr(node, name);
    return attr ? attr->children : NULL;
}
static xmlNode *xmlUtilsFindChild(xmlNode *node, const char *name) {
    for (node = node->children; node; node = node->next) if (node->type == XML_ELEMENT_NODE)
        if (xmlUtilsEqualsStr(node->name, name)) return node;
    return NULL;
}

// endregion

// region Primitves

#define TRACE_PRIMITIVE(node) do { \
    BXML_TRACE("    > '%s'='%s'", ((node) ? ((node)->parent ? (node)->parent->name : (node)->name) : "NULL"), ((node) ? (node)->content : "NULL")); \
} while (0)

static bool board_xml_bool_parse(xmlNode *node, bool def) {
    if (!node) return def;
    TRACE_PRIMITIVE(node);
    if (xmlUtilsEqualsStr(node->content, "false")) return false;
    if (xmlUtilsEqualsStr(node->content, "true")) return true;
    BXML_ERROR("Unexpected value '%s'. Expected [false|true]", node->content);
    return def;
}
static float board_xml_float_parse(xmlNode *node, float def) {
    if (!node) return def;
    TRACE_PRIMITIVE(node);
    return strtof((const char *) node->content, NULL);
}
static long long board_xml_int_parse(xmlNode *node, long def) {
    if (!node) return def;
    TRACE_PRIMITIVE(node);
    return strtoll((const char *) node->content, NULL, 10);
}
static long long board_xml_hexInt_parse(xmlNode *node, long def) {
    if (!node) return def;
    TRACE_PRIMITIVE(node);
    return strtoll((const char *) node->content, NULL, 16);
}
static char board_xml_char_parse(xmlNode *node, char def) {
    if (!node) return def;
    TRACE_PRIMITIVE(node);
    if (node->content[0] == '0' && (node->content[1] == 'x' || node->content[1] == 'X'))
        return (char) board_xml_hexInt_parse(node, 0);
    if (node->content[0] != '\0' && node->content[1] == '\0') return node->content[0];
    BXML_ERROR("Unexpected value '%s'. Expected char", node->content);
    return def;
}
static const char *board_xml_str_parse(xmlNode *node, const char *def) {
    if (!node) return def;
    TRACE_PRIMITIVE(node);
    return (const char *) node->content;
}
static GLcolor32 board_xml_color_parse(xmlNode *node, GLcolor32 def) {
    if (!node) return def;
    TRACE_PRIMITIVE(node);
    const char *str = (char *) node->content;
    if (str[0] != '#') return def;
    str++;
    return strtoul(str, NULL, 16);
}

// endregion

// region IRQ

typedef struct {
    char name;
    uint32_t ctl;
    int index;
} board_xml_irq_t;

#define board_xml_irq_get(avr, irq) avr_io_getirq(avr, (irq).ctl, (irq).index)
static void board_xml_irq_setExternal(avr_t *avr, board_xml_irq_t irq, unsigned long value) {
    avr_ioport_external_t exA = {.name = irq.name, .mask = 1 << irq.index, .value = value};
    avr_ioctl(avr, AVR_IOCTL_IOPORT_SET_EXTERNAL(exA.name), &exA);
}
static board_xml_irq_t board_xml_irq_parse(xmlNode *node) {
    if (!node) return (board_xml_irq_t) {.name = 0, .ctl = 0, .index = 0};
    node = node->children;
    BXML_TRACE(" > '%s'", node->name);
    if (xmlUtilsEqualsStr(node->name, "ioport")) {
        char name = board_xml_char_parse(xmlUtilsFindAttr(node, "name")->children, 0);
        return (board_xml_irq_t) {
                .name = name,
                .ctl = AVR_IOCTL_IOPORT_GETIRQ(name),
                .index = (int) board_xml_int_parse(xmlUtilsFindAttr(node, "index")->children, 0)
        };
    }
    if (xmlUtilsEqualsStr(node->name, "spi")) {
        char name = board_xml_char_parse(xmlUtilsFindAttr(node, "name")->children, 0);
        xmlChar *type = xmlUtilsFindAttr(node, "type")->children->content;
        int index = 0;
        if (xmlUtilsEqualsStr(type, "input")) index = SPI_IRQ_INPUT;
        else if (xmlUtilsEqualsStr(type, "output")) index = SPI_IRQ_OUTPUT;
        else BXML_ERROR("Unexpected type '%s'. Expected [input|output]", type);
        return (board_xml_irq_t) {
                .name = name,
                .ctl = AVR_IOCTL_SPI_GETIRQ(name),
                .index = index
        };
    }
    BXML_ERROR("Unexpected node '%s'. Expected [ioport|spi]", node->name);
    return (board_xml_irq_t) {.name = 0, .ctl = 0, .index = 0};
}

// endregion

// region KEY

typedef struct {
    int code;
} board_xml_key_t;
static board_xml_key_t board_xml_key_parse(xmlNode *node) {
    return (board_xml_key_t) {.code = (int) board_xml_int_parse(xmlUtilsFindAttrVal(node, "code"), 0)};
}

// endregion

// region COMP

#define BOARD_XML_COMP_INIT(f, data, board, avr) void f(void *data, board_t *board, avr_t *avr)
typedef BOARD_XML_COMP_INIT(board_xml_comp_init_t, , , );
#define BOARD_XML_COMP_ONKEY(f, data, board, key, scancode, action, mods) void f(void *data, board_t *board, int key, int scancode, int action, int mods)
typedef BOARD_XML_COMP_ONKEY(board_xml_comp_onKey_t, , , , , , );
#define BOARD_XML_COMP_FREE(f, data) void f(void *data)
typedef BOARD_XML_COMP_FREE(board_xml_comp_free_t, );
typedef struct {
    char *type;
    char *id;
    board_xml_comp_init_t *init;
    board_xml_comp_onKey_t *onKey;
    board_xml_comp_free_t *free;
    void *data;
} board_xml_comp_t;

static void board_xml_comp_parse(board_xml_t *b, xmlNode *node, board_xml_comp_t *comp);
static void board_xml_comp_init(board_xml_comp_t *comp, board_t *board, avr_t *avr) {
    if (comp->init) comp->init(comp->data, board, avr);
}
static void board_xml_comp_onKey(board_xml_comp_t *comp, board_t *board, int key, int scancode, int action, int mods) {
    if (comp->onKey) comp->onKey(comp->data, board, key, scancode, action, mods);
}
static void board_xml_comp_free(board_xml_comp_t *comp) {
    if (comp->free) comp->free(comp->data);
    if (comp->data) free(comp->data);
    comp->data = NULL;
    if (comp->id) free(comp->id);
    comp->id = NULL;
    if (comp->type) free(comp->type);
    comp->type = NULL;
}

// endregion

// region LAYOUT

#define BOARD_XML_LAYOUT_INIT(f, data, board) void f(void *data, board_t *board)
typedef BOARD_XML_LAYOUT_INIT(board_xml_layout_init_t, , );
#define BOARD_XML_LAYOUT_ONLAYOUT(f, data, board) void f(void *data, board_t *board)
typedef BOARD_XML_LAYOUT_ONLAYOUT(board_xml_layout_onLayout_t, , );
#define BOARD_XML_LAYOUT_ONKEY(f, data, board, key, scancode, action, mods) void f(void *data, board_t *board, int key, int scancode, int action, int mods)
typedef BOARD_XML_LAYOUT_ONKEY(board_xml_layout_onKey_t, , , , , , );
#define BOARD_XML_LAYOUT_FREE(f, data) void f(void *data)
typedef BOARD_XML_LAYOUT_FREE(board_xml_layout_free_t, );
typedef struct {
    char *type;
    board_xml_layout_init_t *init;
    board_xml_layout_onLayout_t *onLayout; // must be gll compatible (see ../common/gl_utils.h)
    board_xml_layout_onKey_t *onKey;
    board_xml_layout_free_t *free;
    void *data;
} board_xml_layout_t;

static void board_xml_layout_parse(board_xml_t *b, xmlNode *node, board_xml_layout_t *layout);
static void board_xml_layout_init(board_xml_layout_t *layout, board_t *board) {
    if (layout->init) layout->init(layout->data, board);
}
static void board_xml_layout_onLayout(board_xml_layout_t *layout, board_t *board) {
    if (layout->onLayout) layout->onLayout(layout->data, board);
}
static void board_xml_layout_onKey(board_xml_layout_t *layout, board_t *board, int key, int scancode, int action, int mods) {
    if (layout->onKey) layout->onKey(layout->data, board, key, scancode, action, mods);
}
static void board_xml_layout_free(board_xml_layout_t *layout) {
    if (layout->free) layout->free(layout->data);
    if (layout->data) free(layout->data);
    layout->data = NULL;
    if (layout->type) free(layout->type);
    layout->type = NULL;
}

// endregion

#define board_xml_data_cast(type, ptr, data) type *ptr = (type *) (data)



struct board_xml_t__ {
    size_t compCount;
    board_xml_comp_t *comps;
    board_xml_layout_t layout;
};

static board_xml_comp_t *board_xml_findComp(board_xml_t *board, const char *id) {
    if (!id) return NULL;
    for (size_t i = 0; i < board->compCount; i++)
        if (!strcmp(board->comps[i].id, id)) return &board->comps[i];
    return NULL;
}
#define board_xml_findCompDataByAttr(type, board, idref) ((type *) board_xml_findComp(board, board_xml_str_parse(idref, NULL))->data)


#endif // _BOARD_XML_COMMON_C