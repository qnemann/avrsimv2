#ifndef _LCD_GL_H
#define _LCD_GL_H

#include "lcd.h"
#include "../common/gl_utils.h"


typedef struct {
    uint32_t bg;
    uint32_t char_bg;
    uint32_t shadow;
    uint32_t text;
} lcd_gl_colors_t;


GLvec2f lcd_gl_getSize(lcd_t *b);

void lcd_gl_init();

void lcd_gl_draw(lcd_t *b, lcd_gl_colors_t colors);

#endif
